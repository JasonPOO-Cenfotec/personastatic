package cr.ac.ucenfotec.ui;

import cr.ac.ucenfotec.bl.Persona;

public class UI {

    public static void main(String[] args) {

        Persona persona1 = new Persona(123,"Juan");
        Persona persona2 = new Persona(123,"Juan");
        Persona persona3 = new Persona(789,"María");
        System.out.println( persona1);
        System.out.println( persona2);
        System.out.println(persona3);

        // Ejemplo de métodos estáticos
        System.out.println( "Ejemplo Potencia: " + Math.pow(2,3));
        System.out.println("Ejemplo de casting: " + Integer.parseInt("123"));

        // ¿persona1 es igual a persona2?
        if(persona1.equals(persona2)){
            System.out.println("Sí son iguales!");
        }else{
            System.out.println("No son iguales!");
        }
    }

}
