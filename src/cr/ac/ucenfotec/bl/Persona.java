package cr.ac.ucenfotec.bl;

public class Persona {
    private int id;
    private int cedula;
    private String nombre;
    private static int siguienteId = 1;

    public Persona() {
    }

    public Persona(int cedula, String nombre) {
        this.cedula = cedula;
        this.nombre = nombre;
        id = siguienteId;
        siguienteId++;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String toString() {
        return id + "," + cedula +"," + nombre + ",siguienteId=" + siguienteId;
    }

    public boolean equals(Object obj){
        if(this == obj) return true;
        if(obj == null) return false;
        if(!(obj instanceof Persona)) return false;
        Persona p = (Persona) obj;
        // reglas de negocio
        // dos personas son igual si cumple con
        // que las cedulas son iguales
        if(this.cedula == p.cedula){
            return true;
        }
        return false;
    }
}
